import { StyleSheet } from 'react-native'
import { Colors,Fonts } from 'App/Theme'

export default StyleSheet.create({
  button: {
    width: '100%',
    height: 40,
    backgroundColor: Colors.white,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: Colors.borderBtn,
    overflow: 'hidden',
    zIndex: 4
  },
  text: {
    color: Colors.bottonTxt,
    // fontSize: Fonts.small ,
    fontSize: 12,
    textAlign: 'center',
    fontWeight: 'bold',
    textTransform: 'capitalize'
  },
})
