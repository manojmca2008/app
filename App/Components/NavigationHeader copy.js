import React from 'react'
import { View, Image, TouchableOpacity, StyleSheet, Text } from 'react-native';
import { Header, Left, Right } from 'native-base';
import { ApplicationStyles, Helpers, Images, Metrics, Colors } from 'App/Theme'
import Select from 'App/Components/Select';

const NavigationHeader = ({ props, label }) => (
    <Header style={{ backgroundColor: Colors.header, height: 80, width: '100%', paddingTop: 30 }}>
        <View style={{ justifyContent: 'flex-start', flex: 1, flexDirection: 'row' }}>
            <TouchableOpacity activeOpacity={0.5} onPress={() => props.navigation.goBack()}>
                <Image source={Images.backArrow} style={{ height: 15, width: 20, marginBottom: 5 }} />
            </TouchableOpacity>
            <Text style={styles.label}>{label}</Text>
        </View>
        <View style={{ justifyContent: 'flex-end', flex: 1, flexDirection: 'row' }}>
            <Image source={Images.calendarWright} style={{ height: 30, width: 30, marginRight: 20 }} />
            <Image source={Images.calendarWrong} style={{ height: 30, width: 30, marginRight: 10 }} />
        </View>
    </Header>
)

const styles = StyleSheet.create({
    barIcon: {
        width: 26,
        borderColor: '#1f0a32',
        borderWidth: 2,
        borderLeftWidth: 0,
        borderRightWidth: 0,
        paddingTop: 6,
        paddingBottom: 6
    },
    barIconLine: {
        backgroundColor: '#1f0a32',
        height: 2,
        width: '80%'
    },
    menuIcon: {
        paddingLeft: 10
    },
    label: {
        paddingLeft: 30,
        color: Colors.white,
        fontSize: 20,
        fontWeight: 'bold',
        textTransform: 'capitalize'
    },
});

export default NavigationHeader
