import { StyleSheet } from 'react-native'
import { Colors, ApplicationStyles } from 'App/Theme'
//import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

export default StyleSheet.create({
  select: {
    width: '94%',
    height: 40,
    backgroundColor: Colors.white,
    borderWidth: 2,
    borderColor: Colors.borderDropdown,
    borderRadius: 5,
    //marginBottom: hp('2%'),
    marginHorizontal: '3%',
    justifyContent:'center',
    paddingTop:2
  },
  labelStyle: {
    fontSize: 14,
    fontWeight: 'bold',
    color: Colors.black
  },
  labelBox:{
    flexDirection:'row',
    marginHorizontal: '1%',
    marginBottom: '1.5%'
  },
  placeholderStyle: {
    fontSize: 14,
    fontWeight: 'bold',
    color: Colors.grey,
    paddingLeft: 8
  },
  textStyle: {
    fontSize: 14,
    fontWeight: 'bold',
    color: Colors.black,
    paddingLeft: 8
  }
})
