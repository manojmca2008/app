import React, { useState, useEffect } from "react";
import { Text, View, ActivityIndicator, Image, SafeAreaView, TextInput, FlatList } from 'react-native'
import { connect } from 'react-redux'
import ExampleActions from 'App/Stores/Example/Actions'
import Style from './AppointmentScreenStyle'
import { Images, Layout, Colors } from '../../Theme'
import NavigationHeader from '../../Components/NavigationHeader'
import Select from 'App/Components/Select';
import Button from '../../Components/ButtonBox';
import AppStyle from '../../Theme/ApplicationStyles';
import { HelperService } from '../../Services/Utils/HelperService'
import PageHeader from './PageHeader';
import PageFooter from './PageFooter';

class AppointmentScreen extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      keyword: ''
    }
  }
  componentDidMount() {
    this.props.fetchUser()
  }
  bookAppointment = (slot) => {
    alert('Your slot has been booked');
  }
  render() {
    console.log('slotslist', this.props.slots.slotAppointments[0][0].slot)
    return (
      <SafeAreaView style={Style.mainContainer}>
        <NavigationHeader label="appointment" props={this.props} />
        <View style={Style.searchContainer}>
          <Select
            list={[]}
            onChange={(value) => {
              this.setState({ keyword: value })
            }}
          />
        </View>
        <PageHeader date={this.props.slots && this.props.slots.dates ? HelperService.getFormatedDate(this.props.slots.dates[0]) : ''} />
        <View style={Style.hLine}></View>

        <FlatList
          data={this.props.slots.slotAppointments}
          keyExtractor={(item, index) => {
            return item.id;
          }}
          style={{ paddingBottom: 100 }}
          renderItem={(item) => {
            var item = item.item[0];
            let appointment = item.appointment;
            console.log('my slot', item)
            return (
              <View>
                <View style={{ backgroundColor: 'white', width: '95%', flexDirection: 'row', marginTop: 5, marginBottom: 5, marginHorizontal: '2.5%' }}>
                  <View style={Layout.flexCol3, { width: '20%', paddingTop: 15 }}><Text>{HelperService.getSuffix(item.slot)}</Text></View>
                  <View style={Layout.flexCol3, { width: '55%' }}>
                    {appointment === null ? <Button
                      style={{ paddingVertical: 20 }}
                      title={'Book Appointment'}
                      onPress={() => this.bookAppointment()}
                    /> : <TextInput
                        onChangeText={(value) => this.setState({ name: value })}
                        value={appointment !== null ? HelperService.displayFullName(appointment.patient) : ''}
                        editable={false}
                        style={AppStyle.textInput}
                      />

                    }
                    {(appointment !== null && appointment.appointmentType == 'VIRTUAL') ?
                      <View style={{ position: 'absolute', right: 10, top: 10, flexDirection: 'row' }}>
                        <Image source={Images.virtual} resizeMode="stretch" style={{ height: 20, width: 50 }} />
                        <Image source={Images.virtual} resizeMode="stretch" style={{ height: 20, width: 40 }} /></View> : null}
                  </View>

                  {appointment !== null ? <View style={Layout.flexCol3, { width: '15%', flexDirection: 'row', justifyContent: 'space-between', alignContent: 'flex-end', paddingLeft: 20 }}>
                    <Image source={Images.edit} style={{ height: 25, width: 25, marginTop: 10 }} />
                    <Image source={Images.delete} style={{ height: 25, width: 20, marginLeft: 20, marginTop: 10 }} />
                  </View> : null}
                </View>
                <View style={[Style.hLine,{height: 0.8}]}></View>
              </View>
            )
          }} />
        <PageFooter label={'Patients in Queue:'} value={'4/5'}/>
      </SafeAreaView>
    )
  }
}

const mapStateToProps = (state) => ({
  slots: state.example.user,
  userIsLoading: state.example.userIsLoading,
  userErrorMessage: state.example.userErrorMessage
})

const mapDispatchToProps = (dispatch) => ({
  fetchUser: () => dispatch(ExampleActions.fetchUser()),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppointmentScreen)
