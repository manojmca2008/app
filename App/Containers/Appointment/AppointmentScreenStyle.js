import { StyleSheet } from 'react-native'
import { Helpers, Metrics, Fonts, Colors } from 'App/Theme'

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.background,
  },
  searchContainer: {
    backgroundColor: Colors.header,
    height: 50,
    width: '100%'
  },
  headerContainer: {
    backgroundColor: Colors.white, 
    height: 50, 
    width: '95%', 
    flexDirection: 'row', 
    marginTop: 20, 
    marginHorizontal: '2.5%'
  },
  footerContainer: {
    position: 'absolute', 
    bottom: 0, 
    right:0,
    left:0,
    height: 90, 
    backgroundColor: Colors.white,
    width: '100%' 
  },
  footerInner: {
   // marginHorizontal:'5%',
    width:'90%',
    paddingHorizontal:'5%',
    borderTopWidth: 1, 
    borderTopColor: '#e5e5e5'
  },
  hLine:{
    height: 2, 
    backgroundColor: '#e5e5e5', 
    width: '100%'
  },
  imageStyle: {
    height: 40, 
    width: 40, 
    marginTop: 20, 
    marginLeft: 0,
    marginRight: 10
  },
  footerTxt: {
    textAlign: 'center', 
    paddingVertical: 35 
  },
})
