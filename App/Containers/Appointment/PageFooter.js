import React from 'react'
import { View ,Image,Text} from 'react-native';
import Style from './AppointmentScreenStyle'
import { Layout,Images } from '../../Theme';
import Button from '../../Components/ButtonBox';

const PageFooter = ({ label,value }) => (
  <View style={Style.footerContainer}>
    <View style={[Style.footerInner,Layout.row ]}>
      <Image source={Images.leftArrowGrey} style={Style.imageStyle} />
      <Image source={Images.leftArrowGreen} style={Style.imageStyle} />
        <Text style={Style.footerTxt}>{label} {value}</Text>
      <Image source={Images.rightArrowGrey} style={[Style.imageStyle,{marginLeft: 10}]} />
      <Image source={Images.rightArrowGreen} style={Style.imageStyle} />
    </View>
  </View>
)

export default PageFooter
