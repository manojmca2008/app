import React from 'react'
import { View} from 'react-native';
import Style from './AppointmentScreenStyle'
import { Layout } from '../../Theme';
import Button from '../../Components/ButtonBox';

const PageHeader = ({ date}) => (
    <View style={Style.pageHeader}>
            <View style={Layout.flexCol2, { width: '60%' }}>
              <Button
                style={{ paddingVertical: 20 }}
                title={date}
                onPress={() => alert('Date selected')}
              />
            </View>
            <View style={Layout.flexCol2, { width: '35%' }}>
              <Button
                style={{ paddingVertical: 20, marginHorizontal: 20 }}
                title={'Add walk in'}
                onPress={() => alert('Coming Soon')}
              />
            </View>
          </View>
)

export default PageHeader
