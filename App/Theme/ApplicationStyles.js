
import { StyleSheet } from 'react-native'
import { Colors, ApplicationStyles, Fonts, Metrics, FontFamily } from 'App/Theme'
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

export default StyleSheet.create({

  textInput: {
    width: '98%',
    height: 40,
    backgroundColor: '#ffffff',
    borderWidth: 2,
    borderColor: '#DCDCDC',
    borderRadius: 5,
    paddingHorizontal: 10,
    fontSize: 14,
    marginHorizontal: '1%',
    textTransform: 'capitalize',
    marginBottom: hp('2%'),
  },
   dateInput: {
    width: '98%',
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 5,
    paddingHorizontal: 10,
    color: '#3b4955',
    fontSize: 14,
    fontFamily: 'ConquerorSans',
    marginHorizontal: '1%',
    textTransform: 'capitalize'
  },
  datePlaceholderText:{
    position: 'absolute',
    left: 10,
    color: Colors.grey
  },
  ImageStyle: {
    padding: 15,
    resizeMode: 'stretch',
    alignItems: 'center',
    color: 'grey',
    height:20,
    width:20
  },
  textArea: {
    width: '98%',
    height: 70,
    backgroundColor: '#ffffff',
    borderWidth: 2,
    borderColor: Colors.primary,
    borderRadius: 5,
    paddingHorizontal: 10,
    fontSize: 14,
    fontFamily: 'ConquerorSans',
    marginHorizontal: '1%',
    textTransform: 'capitalize',
    marginBottom: hp('2%'),
  },

  labelText: {
    fontFamily: 'ConquerorSans',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#000000',
    //textTransform: 'capitalize'
  },
  inputLabel: {
    fontFamily: 'ConquerorSans',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#000000',
    paddingLeft: '1%',
    paddingBottom: '1.5%'
    //textTransform: 'capitalize'
  },
  labelValue: {
    paddingLeft: 5,
    fontFamily: 'ConquerorSans',
    fontSize: 14,
    color: '#000000'
  },
  headingText: {
    // fontFamily: 'ConquerorSans-Light',
    fontFamily: 'ConquerorSans',
    fontSize: 18,
    //fontWeight: 'bold',
    color: '#f63f55',
    textTransform: 'capitalize'
  },
  buttonBox: {
    flexDirection: 'row',
    width: '60%',
    justifyContent: 'center',
    alignContent: 'center',
    backgroundColor:'#f63f55',
    borderRadius:20,
    marginVertical:15
  },

  buttonText: {
    backgroundColor: '#f63f55',
    color: 'white',
    //borderRadius: 100,
    fontSize: 18,
    //fontFamily: 'ConquerorSans-Light',
    width: '100%',
    alignSelf: 'center',
    textAlign: 'center',
    overflow: 'hidden',
    paddingVertical: 10,
    textTransform: 'capitalize'
  },
  inputIOS: {
    width: '98%',
    height: 40,
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 5,
    paddingHorizontal: 10,
    color: 'black',
    fontSize: 14,
    fontFamily: 'ConquerorSans',
    //color:'black'
    //shadowOffset: { width: 0, height: 0, },
    //shadowOpacity: .2
  },
  inputAndroid: {
    width: '98%',
    height: 40,
    backgroundColor: '#ffffff',
    borderWidth: 1,
    borderColor: 'grey',
    borderRadius: 5,
    paddingHorizontal: 10,
    color: 'black',
    fontSize: 14,
    fontFamily: 'ConquerorSans',
  },
  pholderText: {
    fontSize: 10,
    // fontFamily: 'ConquerorSans-Light',
    fontFamily: 'ConquerorSans',
    color: 'black'
  },
  tableRowText:{
      color:"#000000",
      fontSize:11,
      fontFamily:'ConquerorSans',
      fontWeight:'bold'
  },

  datePickerContainer: {
    width: '98%',
    height: 40,
    backgroundColor: '#ffffff',
    marginHorizontal:'1%',
    borderRadius: 5,
    borderColor:Colors.primary,
    borderWidth: 2,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 5
  },
  datePickerIcon: {
    color: 'grey',
    fontSize: 24,
  },
  picker: {
   // paddingHorizontal: 0,
  },
  ImageStyleArrow: {
    marginHorizontal: 10,
    resizeMode: 'stretch',
    alignItems: 'center',
    color: Colors.grey,
    fontSize:30
},
arrowBox:{
  position: 'absolute',
  right: 5,
  bottom: 25
},
horizontalLine:{
  borderBottomColor: '#f63f55',
  borderBottomWidth: 1,
  //paddingTop:5
},
topIconBox:{
  flexDirection: 'row',
  paddingBottom: 10,
  position: 'absolute',
  right: 40
},
topIcon:{
  fontSize: 20,
  color: '#f63f55',
  paddingLeft: 15,
  paddingTop: 10
},
centeredView: {
  position: 'absolute',
  right: 20,
  top:60,
  width: 200,
  height: 90
},
centeredView1: {
  position: 'absolute',
  right: 20,
  //top:60,
  bottom:20,
  width: 200,
  height: 90
},
centeredView3: {
  position: 'absolute',
  right: 20,
  //top:60,
  top:160,
  width: 200,
  height: 90
},
centeredView2: {
  position: 'absolute',
  right: 20,
  //top:60,
  bottom:170,
  width: 200,
  height: 90
},
modalView: {
  //margin: 20,
  backgroundColor: "white",
  borderRadius: 10,
  //padding: 35,
  //alignItems: "center",
  shadowColor: "#000",
  shadowOffset: {
    width: 0,
    height: 2
  },
  shadowOpacity: 0.25,
  shadowRadius: 3.84,
  elevation: 5,
  paddingVertical: 30,
  paddingHorizontal: 25
},
modalText: {
  color: '#337ab7',
  fontSize: 16,
  paddingBottom: 20,
  fontWeight: 'bold',
  fontFamily:'ConquerorSans',
}
});
