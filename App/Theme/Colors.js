/**
 * This file contains the application's colors.
 */

export default {
  background: '#ffffff',
  header: '#2ebdc3',
  white:'#ffffff',
  borderDropdown:'#ebebeb',
  borderBtn: '#2ebdc3',
  bottonTxt: '#2ebdc3',

}
