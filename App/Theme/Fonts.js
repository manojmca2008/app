import { StyleSheet } from 'react-native'

const size = {
  h1: 38,
  h2: 34,
  h3: 30,
  input: 18,
  regular: 17,
  medium: 14,
  small: 12,
}

export default StyleSheet.create({
  input: {
    fontSize: size.input,
  },
  small: {
    fontSize: size.regular,
  },
  medium: {
    fontSize: size.medium,
  },
  regular: {
    fontSize: size.regular,
  }
})
