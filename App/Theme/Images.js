/**
 * Images should be stored in the `App/Images` directory and referenced using variables defined here.
 */

export default {
  logo: require('App/Assets/Images/TOM.png'),
  backArrow: require('App/Assets/Images/backArrow.png'),
  calendarWrong: require('App/Assets/Images/calendarWrong.png'),
  calendarWright: require('App/Assets/Images/calendarWright.png'),
  delete: require('App/Assets/Images/delete.png'),
  edit: require('App/Assets/Images/edit.png'),
  virtual: require('App/Assets/Images/virtualGreen.png'),
  leftArrowGreen: require('App/Assets/Images/leftArrowGreen.png'),
  leftArrowGrey: require('App/Assets/Images/leftArrowGrey.png'),
  rightArrowGreen: require('App/Assets/Images/rightArrowGreen.png'),
  rightArrowGrey: require('App/Assets/Images/rightArrowGrey.png'),
}
