import { StyleSheet } from 'react-native'

export default StyleSheet.create({
  container: {
      flex: 1,
      //marginHorizontal: 4,
      //paddingTop: 30,
      //marginBottom: 10,
      backgroundColor: '#ebebeb'
  },
  innerContainer: {
    flex: 1,
    width:'96%',
    //height:'75%',
    marginHorizontal: '2%',
    paddingTop: '5%',
    //marginTop: 10,
    //marginBottom: 10,
    backgroundColor: '#ffffff',
},

inContainer: {
  
  width:'96%',
  //height:'75%',
  marginHorizontal: '2%',
  paddingTop: '5%',
  //marginTop: 10,
  //marginBottom: 10,
  backgroundColor: '#ffffff',
},

  backgroundReset: {
    backgroundColor: 'rgba(0,0,0,0)',
  },
  row: {
    flexDirection: 'row',
  },
  column: {
    flexDirection: 'column',
  },
  center: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  colCenter: {
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  rowCenter: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  spaceAround: {
    flex: 1,
    justifyContent: 'space-around',
  },
  spaceBetween: {
    flex: 1,
    justifyContent: 'space-between',
  },
  fullSize: {
    height: '100%',
    width: '100%',
  },
  fullWidth: {
    width: '100%',
  },
  fullHeight: {
    height: '100%',
  },
  colCross: {
    alignItems: 'center',
    flexDirection: 'column',
  },
  colMain: {
    flexDirection: 'column',
    justifyContent: 'center',
  },

  columnReverse: {
    flexDirection: 'column-reverse',
  },
  crossCenter: {
    alignItems: 'center',
  },
  crossEnd: {
    alignItems: 'flex-end',
  },
  crossStart: {
    alignItems: 'flex-start',
  },
  crossStretch: {
    alignItems: 'stretch',
  },
  fill: {
    flex: 1,
  },
  fillCenter: {
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
  },
  fillCol: {
    flex: 1,
    flexDirection: 'column',
  },
  fillColCenter: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  fillColCross: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
  },
  fillColMain: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  fillColReverse: {
    flex: 1,
    flexDirection: 'column-reverse',
  },
  fillRow: {
    flex: 1,
    flexDirection: 'row',
  },
  fillRowCenter: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  fillRowCross: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
  },
  fillRowMain: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  fillRowReverse: {
    flex: 1,
    flexDirection: 'row-reverse',
  },

  mainCenter: {
    justifyContent: 'center',
  },
  mainEnd: {
    justifyContent: 'flex-end',
  },
  mainSpaceAround: {
    justifyContent: 'space-around',
  },
  mainSpaceBetween: {
    justifyContent: 'space-between',
  },
  mainStart: {
    justifyContent: 'flex-start',
  },
  mirror: {
    transform: [{ scaleX: -1 }],
  },
  rotate90: {
    transform: [{ rotate: '90deg' }],
  },
  rotate90Inverse: {
    transform: [{ rotate: '-90deg' }],
  },
  row: {
    flexDirection: 'row',
  },
  rowCenter: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  rowCross: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  rowMain: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  rowReverse: {
    flexDirection: 'row-reverse',
  },
  scrollSpaceAround: {
    flexGrow: 1,
    justifyContent: 'space-around',
  },
  scrollSpaceBetween: {
    flexGrow: 1,
    justifyContent: 'space-between',
  },
  selfStretch: {
    alignSelf: 'stretch',
  },
  textCenter: {
    textAlign: 'center',
  },
  textJustify: {
    textAlign: 'justify',
  },
  textLeft: {
    textAlign: 'left',
  },
  textRight: {
    textAlign: 'right',
  },
  flexCol2: {
    width: '47%',
    marginHorizontal: '1.5%'
  },
  flexCol4: {
    width: '25%',
    // marginHorizontal: '1%'
  },
  flexCol1: {
    width: '98%',
    marginHorizontal: '1%'
  },

  flexCol3: {
    width: '30%',
    marginHorizontal: '1.5%'

  },
  flexRow: {
    display: 'flex',
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'flex-start'
  },
  flexStart:{
    flex:1,
    justifyContent:'flex-start'
  },
  flexEnd:{
    flex:1,
    justifyContent:'flex-end'
  },
  buttonCol2: {
    //flexDirection: 'row',
    //justifyContent: 'center',
    //alignContent: 'center',
    marginVertical: 20,
    width: '100%'
  },

  buttonCol3: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignContent: 'center',
    marginVertical: 20,
    width: '100%'
  },
  buttonCol1: {
    flex:1,
    justifyContent:'center',
    alignSelf:'center',
    paddingTop: 100,
    //marginVertical: 40,
  },
  buttonFlexRow:{
    flexDirection: 'row',
    justifyContent: 'center',
     alignContent: 'center',
     marginTop: '10%',
     width: '100%'

  }
})
